# Using splot
This script does sPlots (unfolds fit components), and can operate in 2 modes. The number of input files determines which mode is used. No config file is needed for this script

## Standard mode
In this mode of operation, the full input RooDataSet is persisted in the output tree along with the sweights. The only input file is the output of beef (or a file where a RooWorkspace holds a RooDataSet, a RooFitResult and a RooAbsPdf)

## Indexed mode
If two files are given, the so-called indexed mode will be executed. The ouput will be a tree with only the sweights as leaves, which should be used as friend-tree to the input tree (the one located in the second input file). The first input file has to contain the output of a previous fit. The RooDataSet in this file and the input tree from the other file both need an index-branch which determines the order of the output weights. N.B.: This means that no cuts (be careful with implicit ones!) should be made while creating the workspace for the fit.<br>
This method has been developed, since there is a 2GB size limit on RooDataSets. The idea is to only give RooFit the data it needs to do the fit. The rest stays in the input tree. The sweights from this fit are then synchronized again (of course the order of events changes when making a RooDataSet) in this script.
Side remark: has anybody ever understood why RooFit uses RooDataSets instead of TTrees? Why????

## Arguments for the executable
- `-d` : work directory
- `-i` : input file name(s) (separated by ;)
- `-o` : ouput file name
- `-m` : model name (the RooAbsPdf with the full fit model)
- `-p` : fitresult name (only needed if it differs from "fitresult_"+model name+"_"+data set name)
- `-t` : tree name (of the second file in indexed mode; the output tree will be called like this + '_sweights')
- `-r` : input RooDataSet name
- `-w` : input RooWorkspace name
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions:
    - parameters to calculate sweights for (should be more than 2. something like `NSig NBkg`)
