onstart:
  shell("if [ ! -d build ]; then mkdir build; cd build; export CC=gcc; export CXX=g++; else cd build; fi; cmake ..; make; cd ..")

rule result:
  input:
    "toyfit.root"

rule toydata:
  output:
    "toy.root"
  shell:
    'root -b -q src/generateToy.C'

rule create_workspace:
  input:
    "toy.root"
  output:
    "toyws.root"
  shell:
    'build/bin/createWorkspace -i toy.root -c config/config.info -o toyws.root -t toy'

rule fit:
  input:
    "toyws.root"
  output:
    "toyfit.root"
  shell:
    'build/bin/beef -c config/fitconfig.info -i toyws.root -o toyfit.root'