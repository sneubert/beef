/**
  * @class RooPRBox
  * @version 1.0
  * @author Marian Stahl
  * @date  2019-11-16
  * @brief Subbotin p.d.f. with sloped plateau.
  * @details Useful for describing flat or tilted partially reconstructed backgrounds.
  * Parametrized such that mean and width are calculated from masses in the decay chain.
  * See http://pdg.lbl.gov/2017/reviews/rpp2017-rev-kinematics.pdf eqns 48.23 a and b for the calculation.
  * It enables indirect measurements of resonances that constitute partially reconstructed decays.
  */

#ifndef ROOPRBOX
#define ROOPRBOX

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooAbsReal.h"

class RooPRBox : public RooAbsPdf {
public:
  RooPRBox() = default;

  /**
   * @fn RooPRBox(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _M, RooAbsReal& _m12, RooAbsReal& _m1,
                           RooAbsReal& _m2, RooAbsReal& _m3, RooAbsReal& _slope)
   * @brief ctor with parameters
   * @param name
   * @param title
   * @param _x : observable
   * @param _M: mass of decaying particle
   * @param _m12: mass of quasi-stable resonance
   * @param _m1: mass of the particle that has not been reconstructed
   * @param _m2: mass of first reconstructed decay product
   * @param _m3: mass of second reconstructed decay product
   * @param _slope : tilts the plateau. Choose values between -1 and 1
   */
  RooPRBox(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _M, RooAbsReal& _m12, RooAbsReal& _m1,
                    RooAbsReal& _m2, RooAbsReal& _m3, RooAbsReal& _slope);

  /**
   * @fn RooPRBox(const RooPRBox& other, const char* name=0)
   * @brief copy ctor
   * @param other : p.d.f. to copy
   * @param name : name of the new p.d.f.
   */
  RooPRBox(const RooPRBox& other, const char* name=0) ;
  /**
   * @fn virtual TObject* clone(const char* newname) const override
   * @brief ROOT specific clone function
   * @param newname : name of the new p.d.f.
   * @returns A pointer to the p.d.f. as TObject
   */
  virtual TObject* clone(const char* newname) const override { return new RooPRBox(*this,newname); }
  inline virtual ~RooPRBox() = default;

protected:
  RooRealProxy x;
  RooRealProxy M;
  RooRealProxy m12;
  RooRealProxy m1;
  RooRealProxy m2;
  RooRealProxy m3;
  RooRealProxy slope;
  Double_t evaluate() const override;

private:
  ClassDefOverride(RooPRBox,1)
};

#endif
