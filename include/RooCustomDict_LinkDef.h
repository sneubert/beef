#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class RooRelBreitWigner;
#pragma link C++ class RooIpatia2;
#pragma link C++ class RooJohnsonSU;
#pragma link C++ class RooHILLdini;
#pragma link C++ class RooHORNSdini;
#pragma link C++ class RooTiltedSubbotin;
#pragma link C++ class RooPRBox;
#pragma link C++ class RooPRHorns;
#pragma link C++ class RooPRHill;

#pragma link C++ defined_in "RooRelBreitWigner.h";
#pragma link C++ defined_in "RooIpatia2.h";
#pragma link C++ defined_in "RooJohnsonSU.h";
#pragma link C++ defined_in "RooHILLdini.h";
#pragma link C++ defined_in "RooHORNSdini.h";
#pragma link C++ defined_in "RooTiltedSubbotin.h";
#pragma link C++ defined_in "RooPRBox.h";
#pragma link C++ defined_in "RooPRHorns.h";
#pragma link C++ defined_in "RooPRHill.h";

#endif /* __CINT__ */
