//from std
#include <string>
#include <iostream>
#include <vector>
//from BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
//from ROOT
#include "TString.h"
#include "TFile.h"
#include "TStopwatch.h"
//from ROOFIT
#include "RooAbsPdf.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooFitResult.h"
#include "RooRealVar.h"
#include "RooSimultaneous.h"
#include "RooWorkspace.h"
//local
#include "plotter.h"
#include "debug_helpers.h"
#include "IOjuggler.h"

#ifndef BEEF_GIT_HASH
#define BEEF_GIT_HASH " "
#endif

namespace pt = boost::property_tree;

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner(){return 0;}

int main(int argc, char** argv){

  //////////////////////////////////////////////////////////
  ///  parse command-line options and get first objects  ///
  //////////////////////////////////////////////////////////
  TStopwatch clock;
  clock.Start();
  auto options = IOjuggler::parse_options(argc, argv, "c:d:hi:o:r:v:w:p:");
  MessageService msgsvc("beef",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current beef git hash: " + static_cast<std::string>(BEEF_GIT_HASH));
  //open file, check for absolute path and a path with prepended workdir
  const auto wd     = options.get<std::string>("workdir");
  const auto ofn    = options.get<std::string>("outfilename");
  const auto prefix = options.get<std::string>("prefix","");
  const auto wsn    = options.get<std::string>("wsname");
  const auto dsn    = options.get<std::string>("dsname");
  const auto w      = IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(options.get<std::string>("infilename"),wd),wsn);

  ///////////////////////////////
  ///  runtime configuration  ///
  ///////////////////////////////
  pt::ptree configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  IOjuggler::replace_stuff_in_ptree(configtree,"{prefix}",prefix,"");
  //append and replace stuff in ptree
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);
  IOjuggler::SetRootVerbosity(configtree.get_optional<int>("suppress_RootInfo"));

  ////////////////////////////
  ///  get data and model  ///
  ////////////////////////////
  const auto DATA = IOjuggler::get_wsobj<RooDataSet>(w,dsn);
  const auto modelname = configtree.get_optional<std::string>("pdf");
  if(!modelname) throw std::runtime_error("\"pdf\" not found in config file");
  std::string model_name_for_plot = *modelname;

  const auto sim_node = configtree.get_child_optional("simultaneous");
  if(sim_node){//build simultaneous model
    model_name_for_plot = configtree.get("sim_pdf","sim_model");
  }
  const auto PDF = IOjuggler::get_wsobj<RooAbsPdf>(w,model_name_for_plot);

  //////////////////////////
  /// output to disk : plots
  // create control plot
  const auto plotnodeopt = configtree.get_child_optional("plots");
  if(plotnodeopt){
    for(const auto& plot : *plotnodeopt)
      if(plot.second.get_optional<std::string>("var"))//there could be REPLACE/APPEND nodes
        plotter(w,*DATA,PDF,ofn,wd,prefix,plot.second,msgsvc);
  }
  else if(!plotnodeopt && sim_node){
    auto titer = IOjuggler::get_wsobj<RooCategory>(w,static_cast<RooSimultaneous*>(PDF)->indexCat().GetName())->typeIterator();
    while(const auto vcat = titer->Next())
      if(const auto& simpnode = configtree.get_child_optional("plots_"+static_cast<std::string>(vcat->GetName())))
        for(const auto& sim_plot : *simpnode)
          if(sim_plot.second.get_optional<std::string>("var"))
            plotter(w,*DATA,PDF,ofn,wd,prefix,sim_plot.second,msgsvc);
  }

  return 0;
}
