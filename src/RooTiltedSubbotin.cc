/**
  * @file RooTiltedSubbotin.cc
  * @version 1.0
  * @author Marian Stahl
  * @date  2018-12-25
  */

#include "RooTiltedSubbotin.h"

ClassImp(RooTiltedSubbotin)

RooTiltedSubbotin::RooTiltedSubbotin(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _mu,
                                     RooAbsReal& _width, RooAbsReal& _beta, RooAbsReal& _slope) :
  RooAbsPdf(name,title), x("x","observable",this,_x), mu("mu","mean",this,_mu), width("width","width",this,_width),
  beta("beta","beta",this,_beta), slope("slope","slope",this,_slope){ }

RooTiltedSubbotin::RooTiltedSubbotin(const RooTiltedSubbotin& other, const char* name) :
  RooAbsPdf(other,name), x("x",this,other.x), mu("mu",this,other.mu), width("width",this,other.width),
  beta("beta",this,other.beta), slope("slope",this,other.slope){ }

Double_t RooTiltedSubbotin::evaluate() const {
  //the normalisation would look horrible, but fortunately RooFit does that for us
  return std::exp(slope*x/width-std::pow((std::abs(x-mu)/width),beta));
}



