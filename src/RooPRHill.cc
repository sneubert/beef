#include "RooPRHill.h"
#include "TMath.h"

templateClassImp(RooPRHill)

RooPRHill::RooPRHill(const char *name, const char *title,
                            RooAbsReal& _m, RooAbsReal& _M, RooAbsReal& _m12, RooAbsReal& _m1, RooAbsReal& _m2, RooAbsReal& _m3,
                            RooAbsReal& _xi, RooAbsReal& _sigma) :
  RooAbsPdf(name, title),
  m("m", "Observable", this, _m),
  M("M","Mass of decaying particle",this,_M),
  m12("m12","Mass of (12) resonance",this,_m12),
  m1("m1","Mass of missing particle",this,_m1),
  m2("m2","Mass of 2nd daughter",this,_m2),
  m3("m3","Mass of 3rd daughter",this,_m3),
  xi("xi", "xi", this, _xi),
  sigma("sigma", "sigma", this, _sigma){}

RooPRHill::RooPRHill(const RooPRHill& other, const char* name) :
  RooAbsPdf(other, name),
  m("m", this, other.m), M("M",this,other.M), m12("m12",this,other.m12), m1("m1",this,other.m1), m2("m2",this,other.m2), m3("m3",this,other.m3),
  xi("xi", this, other.xi), sigma("sigma", this, other.sigma) {}

Double_t RooPRHill::evaluate() const {

  auto const E2Star = (m12*m12-m1*m1+m2*m2)/(2*m12);//energy of 2 in the 12 RF
  auto const E3Star = (M*M-m12*m12-m3*m3)/(2*m12);//energy of 3 in the 12 RF
  auto const SumE   = E2Star+E3Star;
  auto const term1  = SumE*SumE;
  auto const term2  = std::sqrt(E2Star*E2Star-m2*m2);
  auto const term3  = std::sqrt(E3Star*E3Star-m3*m3);
  auto const t2mt3  = term2-term3;
  auto const t2pt3  = term2+term3;
  auto const b_new  = std::sqrt(term1-t2mt3*t2mt3);
  auto const a_new  = std::sqrt(term1-t2pt3*t2pt3);

  double firstG1 = (2*exp(-((a_new-m)*(a_new-m)/(2*(sigma*sigma))))*sigma*(b_new-m)
                   +2*exp(-((b_new-m)*(b_new-m)/(2*(sigma*sigma))))*sigma*(-a_new+m)
                   -sqrt(2*TMath::Pi())*(a_new*b_new+(sigma*sigma)-(a_new+b_new)*m+(m*m))*TMath::Erf((-a_new+m)/(sqrt(2)*sigma))
                   +sqrt(2*TMath::Pi())*(a_new*b_new+(sigma*sigma)-(a_new+b_new)*m+(m*m))*TMath::Erf((-b_new+m)/(sqrt(2)*sigma)))/(2*sqrt(2*TMath::Pi()));
  return  fabs((1-xi)/(b_new-a_new)*m+(b_new*xi-a_new)/(b_new-a_new))*fabs(firstG1);

}
