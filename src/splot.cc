//from std
#include <string>
#include <vector>
//from ROOT
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TStopwatch.h"
#include "TMatrixD.h"
//from ROOFIT
#include "RooAbsPdf.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooRealVar.h"
#include "RooWorkspace.h"
//from ROOSTATS
#include "RooStats/SPlot.h"
#include <IOjuggler.h>
#include "ProgressBar.h"

#ifndef BEEF_GIT_HASH
#define BEEF_GIT_HASH " "
#endif

namespace pt = boost::property_tree;

//suppress RooBanner https://root.cern.ch/doc/master/RooBanner_8cxx_source.html
int doBanner(){return 0;}

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  //don't spam!
  RooMsgService::instance().setGlobalKillBelow(static_cast<RooFit::MsgLevel>(4));

  auto options = IOjuggler::parse_options(argc, argv, "d:hi:m:o:p:r:t:v:w:","nonoptions: parameters to calculate sweights for",2);
  MessageService msgsvc("splot",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current beef git hash: " + static_cast<std::string>(BEEF_GIT_HASH));
  //open file, check for absolute path and a path with prepended workdir
  const auto wd     = options.get<std::string>("workdir");
  const auto ifn    = options.get<std::string>("infilename");
  const auto ofn    = options.get<std::string>("outfilename");
  const auto tn     = options.get<std::string>("treename");
  const auto wsn    = options.get<std::string>("wsname");
  const auto dsn    = options.get<std::string>("dsname");
  const auto modn   = options.get<std::string>("weightfile");
  const auto frname = options.get<std::string>("prefix","fitresult_"+modn+"_"+dsn);

  std::vector<std::string> input_files;
  boost::algorithm::split(input_files, ifn, boost::algorithm::is_any_of(";"));
  if(input_files.size() > 2u)
    throw std::runtime_error("Expecting one or two input files");

  //this script can run in two modes. the modes are determined implicitly from the number of input files.
  // in the so-called indexed mode, the ouput is a tree with only nspec leaves that can be used as
  // friend-tree to the input tree (located in the second input file). this input tree needs an index-branch.
  // that same index needs also to be in the input RooDataSet. by default this RooDataSet is spit out by beef
  // in a workspace along with a RooFitResult and a model PDF.
  // In the non-indexed mode of operation, the full input RooDataSet is persisted along with the sweights.
  const bool run_indexed = input_files.size() == 2 ? true : false;

  // get some stuff from the workspace (also things we need only later)
  const auto w      = IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(input_files[0],wd),wsn);
  const auto data   = IOjuggler::get_wsobj<RooDataSet>(w,dsn,msgsvc);
  const auto pdf    = IOjuggler::get_wsobj<RooAbsPdf>(w,modn,msgsvc);
  const auto fr     = IOjuggler::get_wsobj<RooFitResult>(w,frname,msgsvc);

  //make a copy, just to be safe
  RooDataSet    ds(*data,"copied_data");
  delete data;

  //parse nonoptions
  auto eas = options.get_child_optional("extraargs");
  const auto nspec = (*eas).size();
  msgsvc.debugmsg("Size of extraargs: "+std::to_string(nspec));//this should be larger or equal to 2, otherwise parse_options will throw

  const auto numevents = ds.numEntries();
  msgsvc.infomsg(TString::Format("Entries in %s: %d",dsn.data(),numevents));

  ProgressBar pg;
  std::vector<int> new_sequence;
  if(run_indexed){
    // first: match the index branch and the index variable in the input samples
    auto tree = IOjuggler::get_obj<TTree>(IOjuggler::get_file(input_files[1],wd,"update",msgsvc),tn,msgsvc);
    tree->SetBranchStatus("*",0);
    tree->SetBranchStatus("index",1);
    int idx;
    tree->SetBranchAddress("index",&idx);

    std::vector<std::pair<int,int>> index_ds;
    std::vector<int> index_tree;

    if(numevents != tree->GetEntries())
    throw std::runtime_error("This shouldn't happen. check that both samples have the same number of events");

    pg.start(numevents,msgsvc);
    msgsvc.infomsg("Filling index vector from Tree");
    //two loops should be faster (depending on where the stuff is in memory)
    for(int ie = 0; ie < numevents; ie++){
      tree->GetEntry(ie);
      pg.update(ie);
      index_tree.push_back(idx);
    }

    pg.stop();
    pg.start(numevents,msgsvc);
    msgsvc.infomsg("Filling pairs of number in sequence and index from DataSet");
    int deubgctr = 0;
    for(int ie = 0; ie < numevents; ie++){
      pg.update(ie);
      auto index_inds = static_cast<int>(ds.get(ie)->getRealValue("index"));
      if(index_inds != index_tree[ie] && deubgctr < 20){
        msgsvc.debugmsg(TString::Format("At entry %d Index in tree %d Index in ds %d",ie,index_tree[ie],index_inds));
        deubgctr++;
      }
      index_ds.emplace_back(ie,index_inds);
    }
    pg.stop();

    msgsvc.debugmsg("Sorting DataSet vector by index number");
    std::sort(index_ds.begin(),index_ds.end(),[](std::pair<int,int> a,std::pair<int,int> b){return a.second < b.second;});
    msgsvc.debugmsg("First "+std::to_string(index_ds[0].second)+" Second "+std::to_string(index_ds[1].second));

    pg.start(numevents,msgsvc);
    msgsvc.infomsg("Searching for Tree indices in DataSet and determining the order of writing output events");
    for(int ie = 0; ie < numevents; ie++){
      new_sequence.push_back((*std::lower_bound(index_ds.begin(),index_ds.end(),index_tree[ie],[](const auto& a, int it){return a.second < it;})).first);
      pg.update(ie);
    }
    pg.stop();

    if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG){
      msgsvc.debugmsg("debugloop");
      deubgctr = 0;
      for(int ie = 0; ie < numevents; ie++){
        auto index_inds = static_cast<int>(ds.get(new_sequence[ie])->getRealValue("index"));
        if(index_inds != index_tree[ie] && deubgctr < 20){
          msgsvc.debugmsg(TString::Format("This shouldn't happen! At entry %d Index in tree %d Index in ds %d",ie,index_tree[ie],index_inds));
          deubgctr++;
        }
      }
      msgsvc.debugmsg("done with debugloop");
    }
    if(new_sequence.size() != numevents)
      throw std::runtime_error("Something went wrong with determining the order of output events");
  }

  //prepare output
  std::string const oloc = wd.empty() ? ofn : wd+"/"+ofn;
  IOjuggler::dir_exists(oloc);
  TFile ff(oloc.data(),"RECREATE");
  TTree sweighttree((tn+"_sweights").data(),(tn+"_sweights").data());
  std::vector<double> sweights(nspec,0.);
  std::vector<std::string> exn;
  for(const auto& ea : *eas)
    exn.push_back(ea.second.data());
  for(unsigned int i = 0; i < nspec; i++){
    msgsvc.debugmsg("Adding new branch: "+exn[i]+"_sw");
    sweighttree.Branch((exn[i]+"_sw").data(),&sweights[i],(exn[i]+"_sw/D").data());
  }
  //we only need this vector in the non-indexed case, but need to declare it in this scope to use it later
  std::vector<double> ds_vars(ds.get(0)->getSize(),0);
  if(!run_indexed){
    //transfer ds structure to tree
    auto iter = ds.get(0)->createIterator();
    unsigned int index = 0;
    while(auto var = iter->Next()){
      sweighttree.Branch(var->GetName(),&ds_vars[index],TString::Format("%s/D",var->GetName()).Data());
      index++;
    }
  }

  //Now we can get the sWeights. we need to set all shape parameters constant and put the yield parameters in a RooArgList
  RooArgList extended_parameters;
  //wtf roofit? why does static_cast<RooArgSet>(fitresult->floatParsFinal()).createIterator() give complete nonsense?
  const auto afps = static_cast<RooArgSet>(fr->floatParsFinal());
  for(const auto& ex : exn){
    if(auto found_par = afps.find(ex.data()); found_par != nullptr)
      extended_parameters.add(*static_cast<RooRealVar*>(found_par));
    else throw std::runtime_error("Did not find "+ex+" in the floating parameters of the fit result");
  }
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    extended_parameters.Print();

  msgsvc.debugmsg("Doing the splot now");//what follows is basically copy pasted from https://root.cern.ch/doc/master/SPlot_8cxx_source.html

  RooArgList* constParameters = (RooArgList*)pdf->getParameters(ds) ;
  constParameters->remove(extended_parameters, kTRUE, kTRUE);
  std::vector<RooRealVar*> constVarHolder;
  for(int i = 0; i < constParameters->getSize(); i++){
    RooRealVar* varTemp = ( dynamic_cast<RooRealVar*>( constParameters->at(i) ) );
    if(varTemp &&  varTemp->isConstant() == 0 ) {
      varTemp->setConstant();
      constVarHolder.push_back(varTemp);
    }
  }

  RooArgList yields = *(RooArgList*)extended_parameters.snapshot(kFALSE);
  // first calculate the pdf values for all species and all events
  std::vector<RooRealVar*> yieldvars ;
  RooArgSet* parameters = pdf->getParameters(&ds) ;

  std::vector<double> yieldvalues ;
  for (int k = 0; k < nspec; ++k) {
    RooRealVar* thisyield = dynamic_cast<RooRealVar*>(yields.at(k)) ;
    if (thisyield) {
      RooRealVar* yieldinpdf = dynamic_cast<RooRealVar*>(parameters->find(thisyield->GetName() )) ;
      if (yieldinpdf) {
        msgsvc.infomsg(TString::Format("yield in pdf: %s %9.6g",yieldinpdf->GetName(),thisyield->getVal()));
        yieldvars.push_back(yieldinpdf) ;
        yieldvalues.push_back(thisyield->getVal()) ;
      }
    }
  }

  std::vector<std::vector<double> > pdfvalues(numevents,std::vector<double>(nspec,0));
  // set all yield to zero

  for(int m=0; m<nspec; ++m)
    yieldvars[m]->setVal(0) ;

  // For every event and for every specie, calculate the value of the component pdf for that specie
  // by setting the yield of that specie to 1 and all others to 0.  Evaluate the pdf for each event and store the values.
  auto pdfvars = pdf->getVariables();
  auto vars(ds.get());

  pg.start(numevents,msgsvc);
  msgsvc.infomsg("Getting p.d.f values for each species");
  for (int ievt = 0; ievt <numevents; ievt++) {
    pg.update(ievt);
    *pdfvars = *(ds.get(ievt));
    for(int k = 0; k < nspec; ++k){
      //Check that range of yields is at least (0,1), and fix otherwise
      if(yieldvars[k]->getMin() > 0) {
        msgsvc.warningmsg(TString::Format("Minimum Range for %s must be >= 0.... Setting min range to 0",yieldvars[k]->GetName()));
        yieldvars[k]->setMin(0);
      }
      if(yieldvars[k]->getMax() < 1){
        msgsvc.warningmsg(TString::Format("Maximum Range for %s must be <= 1.... Setting max range to 1",yieldvars[k]->GetName()));
        yieldvars[k]->setMax(1);
      }
      // set this yield to 1
      yieldvars[k]->setVal( 1 ) ;
      // evaluate the pdf
      double f_k = pdf->getVal(*vars) ;
      pdfvalues[ievt][k] = f_k ;
      if( !(f_k>1 || f_k<1) )
        msgsvc.warningmsg(TString::Format("Strange pdf value: %d %d %9.6g",ievt,k,f_k));
      yieldvars[k]->setVal( 0 ) ;
    }
  }
  delete pdfvars;

  // Make a TMatrixD to hold the covariance matrix.
  TMatrixD covInv(nspec, nspec);
  for (int i = 0; i < nspec; i++) for (int j = 0; j < nspec; j++) covInv(i,j) = 0;
  pg.stop();
  pg.start(numevents,msgsvc);
  msgsvc.infomsg("Calculate the inverse covariance matrix");
  for (int ievt = 0; ievt < numevents; ++ievt)  {
    pg.update(ievt);
    // Calculate contribution to the inverse of the covariance matrix. See BAD 509 V2 eqn. 15
    // Sum for the denominator
    double dsum(0);
    for(int k = 0; k < nspec; ++k)
      dsum += pdfvalues[ievt][k] * yieldvalues[k];

    for(int n=0; n<nspec; ++n)
      for(int j=0; j<nspec; ++j){
        //if(includeWeights == kTRUE)
        //  covInv(n,j) +=  ds.weight()*pdfvalues[ievt][n]*pdfvalues[ievt][j]/(dsum*dsum) ;
        //else
        covInv(n,j) +=  pdfvalues[ievt][n]*pdfvalues[ievt][j]/(dsum*dsum) ;
      }
  }

  // Invert to get the covariance matrix
  if (covInv.Determinant() <=0)  {
    msgsvc.errormsg("SPlot Error: covariance matrix is singular; I can't invert it!");
    covInv.Print();
    return 1;
  }

  TMatrixD covMatrix(TMatrixD::kInverted,covInv);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG){
    covInv.Print();
    covMatrix.Print();
  }
  // calculate for each event the sWeight (BAD 509 V2 eq. 21)
  pg.stop();
  pg.start(numevents,msgsvc);
  msgsvc.infomsg("Calculating sweight");

  for(int ievt = 0; ievt < numevents; ++ievt) {
    pg.update(ievt);
    // sum for denominator
    double dsum{0};
    for(int k = 0; k < nspec; ++k)
      dsum += (run_indexed ? pdfvalues[new_sequence[ievt]][k] : pdfvalues[ievt][k]) * yieldvalues[k];
    // covariance weighted pdf for each species
    for(int n=0; n<nspec; ++n) {
      double nsum{0} ;
      for(int j=0; j<nspec; ++j)
        nsum += covMatrix(n,j) * (run_indexed ? pdfvalues[new_sequence[ievt]][j] : pdfvalues[ievt][j]) ;

      //Add the sWeights here!!
      sweights[n] = dsum != 0. ? nsum/dsum : 0.;

      if( !(fabs(sweights[n])>=0 ) ) {
        msgsvc.errormsg(TString::Format("error: %.3f",sweights[n]));
        return 2;
      }

    }
    if(!run_indexed){
      auto iter = ds.get(ievt)->createIterator();
      auto index = 0u;
      while(auto var = iter->Next()){
        ds_vars[index] = static_cast<RooRealVar*>(ds.get(ievt)->find(var->GetName()))->getVal();
        index++;
      }
    }
    sweighttree.Fill();
  }
  pg.stop();
  sweighttree.Write();
  ff.Close();

  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();

  return 0;
}
